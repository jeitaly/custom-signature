# Custom-signature

Questo progetto contiene il codice necessario per installare nel proprio sito WordPress una pagina personalizzata per generare la firma email della propria JE.

## Installazione

1. Scarica questa cartella. Nella barra laterale, vai sulla voce *"Downloads"* e poi premi su *"Download repository"*. Si avvierà il download automatico della repository come archivio ```.zip```.

2. Apri una connessione FTP con il tuo server, per esempio con FileZilla.

3. Entra nella cartella principale del tuo sito. La puoi riconoscere dalla presenza di sotto-cartella come ```wp-admin```, ```wp-includes```, e ```wp-content```.

4. Crea una nuova cartella con il nome del percorso a cui vuoi che sia accessibile la pagina. Ad esempio, chiama la cartella ```firma``` se vuoi che la pagina sia ```http://ildominiodellatuaje/firma```.

5. Copia il contenuto dell'archivio nella nuova cartella creata.

Se l'installazione è andata a buon fine, connettendoti al link ```http://ildominiodellatuaje/firma``` dovresti vedere il form di creazione firma email.

## Personalizzazione

Per personalizzare la firma per la tua JE, apri il file ```index.php``` in un qualsiasi editor di testo (consigliato SublimeText). All'interno del file troverai delle porzioni di testo come, per esempio, ```<< Inserisci il nome della tua JE >>``` che dovrai sostituire con l'informazione richiesta.

Di seguito si riportano i dati richiesti:

* Nome della tua JE (e.g. ```JEABC```)

* Link alla pagina FB della tua JE

* Link alla pagina LinkedIn della tua JE

* Link alla pagina Instagram della tua JE

* Link al sito della tua JE (e.g. ```http://jeabc.xyz```)

* Link al logo della tua JE che vuoi fare apparire nella firma (e.g. ```http://jeabc.xyz/logo.png```)

* Dominio della tua JE (e.g. ```jeabc.xyz```)

* Colore principale in RGB (e.g. ```rgb(10, 20, 30)```)

* Sede della tua JE

Dopo aver modificato il file con le informazioni della tua JE, sostituisci il file sul server. Prova a generare una firma e verifica che tutte le modifiche siano state effettuate correttamente.