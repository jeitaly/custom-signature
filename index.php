<!DOCTYPE html>
<html>
<head>
	<title>Firma mail custom</title>
    <!-- Bootstrap Material Design -->
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto">
	<link rel="stylesheet" type="text/css" href="./index.css">
	<style type="text/css">


		.center-p{
			text-align:center;
		}
		.alert {
			font-family: "Roboto";
			color: red;
		}
		.hide {
			color: transparent;
		}
		
		*{
	font-family: 'Roboto'; 
    font-weight: normal; 
    font-style: normal;
  }
  
	</style>
	<!-- <script type="text/javascript">

			$("input").change(function () {
    			var value = this.value;
    			if (value == "") {


        			$(this).css("background-color", "red");
    			}
    		}).trigger("change");

    	</script> -->
    </head>
    <body class="text-center">
    	<?php if(isset($_POST["submit"])) {

    		$nome = $_POST["nome"];
    		$cognome = $_POST["cognome"];
    		$ruolo = $_POST["ruolo"];
    		$cell = $_POST["cell"];
    		if ($nome == "" || $cognome == "" || $ruolo == "" || $cell == "") {
    			header("location:http://juniorenterprises.it/firma/index.php?required=1");
    		}
    		?>

    		<div align="center">
    			<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate!important; table-layout: fixed; text-size-adjust: none !important; -ms-text-size-adjust: none !important; -webkit-text-size-adjust: none !important;
    			" width="450">
    			<tbody>
    				<tr>
    					<td class="logo-td" align="center" valign="top" width="130" margin-right="13px">
    						<p style="margin-bottom: 10px; margin-right: 3px; margin-left:-3px; font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 14px; text-align: center;">
    							<a style="text-decoration:none" href="<< Inserisci qui il link al sito della tua JE >>" class="clink logo-container">
    								<img src="<< Inserisci qui il link al logo della tua JE >>" alt="<< Inserisci qui il nome della tua JE >>" border="0" class="sig-logo" height="100" width="110">

    							</a>
    						</p>
    						<p class="social-list" style="font-size: 0px; line-height: 0; font-family: Helvetica, Arial, sans-serif;">
    							<a style="text-decoration: none; display: inline;" class="social signature_twitter-target sig-hide" href="<< Inserisci qui il link alla pagina FB della tua JE >>">
    								<img width="16" style="margin-bottom:2px; border:none; display:inline;" height="16" data-filename="twitter.png" src="https://<< Inserisci qui il dominio della tua JE >>/firma-custom/assets/facebook.png" alt="Facebook">
    							</a>
    							<span style="white-space: nowrap; display: inline;" class="signature_twitter-sep social-sep">
    								<img src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" width="5">
    							</span>
								
								
								<a style="text-decoration: none; display: inline;" class="social signature_twitter-target sig-hide" href="<< Inserisci qui il link alla pagina LinkedIn della tua JE >>">
    								<img width="16" style="margin-bottom:2px; border:none; display:inline;" height="16" data-filename="twitter.png" src="https://<< Inserisci qui il dominio della tua JE >>/firma-custom/assets/linkedin.png" alt="Linkedin">
    							</a>
								
								<span style="white-space: nowrap; display: inline;" class="signature_twitter-sep social-sep">
    								<img src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" width="5">
    							</span>
    							<a style="text-decoration: none; display: inline;" class="social signature_twitter-target sig-hide" href="<< Inserisci qui il link alla pagina Instagram della tua JE">
    								<img width="16" style="margin-bottom:2px; border:none; display:inline;" height="16" data-filename="twitter.png" src="https://<< Inserisci qui il dominio della tua JE >>/firma-custom/assets/instagram.png" alt="Instagram">
    							</a>
								
    							
    							
    						</p>
    					</td>
    					<!--Barra verticale-->
    					<td align="left" valign="top" nowrap="nowrap" class="spacer-td" width="16" style="border-left: 6px solid  << Inserisci qui il colore principale del tema in RGB >> ; padding-left:3px;">
    						<img src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" width="10">
    					</td>
    					<td align="left" valign="top" nowrap="nowrap" class="content-td" width="354" style="padding-top:9px;">
    						<div class="content-pad" style="">
    							<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; line-height: 14px; color: rgb(33, 33, 33); margin-bottom: 10px;">
    								<span style="font-weight: bold; color: rgb(34, 34, 33); display: inline;" class="txt signature_name-target sig-hide"><?php echo $nome." ".$cognome?></span>
    								<span class="title-sep sep" style="display: inline;">/</span> <span style="color: rgb(34, 34, 33); display: inline;" class="txt signature_jobtitle-target sig-hide"><?php echo $ruolo?></span>
    								<span class="email-sep break" style="display: inline;"><br></span>
									<span style="color:  << Inserisci qui il colore principale del tema in RGB >> ; display: inline;">Mobile: </span><span style="color: rgb(34, 34, 33); display: inline;" class="txt signature_mobilephone-target sig-hide"><?php echo $cell?></span><br>
									<a class="link email signature_email-target sig-hide" href="mailto:<?php echo strtolower($nome).".".strtolower($cognome)."@jeitaly.org"?>" style="color: rgb(34,34,33); text-decoration: none;">
									<span style="color:  << Inserisci qui il colore principale del tema in RGB >> ; display: inline;">E-mail:</span><span style="color: rgb(34, 34, 33); display: inline;" class="txt signature_mobilephone-target sig-hide"> <?php echo strtolower($nome).".".strtolower($cognome)."@<< Inserisci qui il dominio della tua JE >>"?></span></a>
    							</p>
    							<p class="company-info" style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; line-height: 14px; margin-bottom: 10px;">
    								<span style="font-weight: bold; color: rgb(34, 34, 33); display: inline;" class="txt signature_companyname-target sig-hide"><< Inserisci qui il nome della tua JE >></span>
    								<span style="color: rgb(34, 34, 33);" class="txt signature_fax-target sig-hide"></span>
    								<span class="address-sep break" style="display: inline;"><br></span> <span style="color: rgb(34, 34, 33); display: inline;" class="txt signature_address-target sig-hide"><< Inserisci qui la sede della tua JE >></span>
    								<span class="website-sep break" style="display: inline;"><br></span>
    								<a class="link signature_website-target sig-hide" href="<<Inserisci qui il link del sito della JE>>" style="color:  << Inserisci qui il colore principale del tema in RGB >> ; text-decoration: none; display: inline;"><< Inserisci qui il sito web della tua JE >></a>
    							</p>
    						</div>

    					</td>
    				</tr>
    				<tr>
    					<td colspan="3">

    					</td>
    				</tr>
    				<tr>
    					<td colspan="3">


    					</td>
    				</tr>
    				<tr>
    					<td colspan="3"><p style="color: rgb(34, 34, 33); font-size: 9px; line-height: 12px; margin-top: 10px;" class="txt signature_disclaimer-target">IT /
Questo messaggio, ed eventuali allegati, &egrave; diretto all&#8217;esclusiva attenzione del destinatario. Il messaggio, ed eventuali allegati, contiene informazioni riservate e/o privilegiate. Qualora Lei non fosse il destinatario, La invitiamo a darcene immediata comunicazione e ad eliminare tutte le copie del messaggio, comprensive degli allegati. La preghiamo inoltre di non leggere, stampare, conservare, copiare, inoltrare o divulgare il messaggio o alcuna sua parte. Qualunque suo utilizzo non autorizzato &egrave; proibito e pu&ograve; configurare un illecito, in conformit&agrave; al Regolamento (UE) 2016/679 (GDPR) ed alle previsioni del D.Lgs. 196/2003, cos&igrave; come modificato dal D.Lgs. 101/2018.
<br />
<br />
EN /
This email and any attached files are only for the attention of the intended recipient. This email and any eventual contain confidential and/or privileged information. If you are not the intended recipient, please notify us immediately and delete all copies of the email, including any attachments. Please do not read, print, retain, copy, forward or disclose this message or any part of it. Any such unauthorized use is strictly forbidden and may be unlawful, pursuant to Regulation (EU) 2016/679 (GDPR) and national provisions of Legislative Decree No. 196/2003 as amended by Legislative Decree No. 101/2018.</p></td>
    				</tr>
					
    			</tbody>
    		</table>
    	</div>

    	<?php } else {?>
    	<div align="center" class="form-container">
			<form action="index.php" method="post" class="form-signin">
      			<h1 class="h3 mb-3 font-weight-normal">Generatore di firma email</h1>
      			<label for="nome" class="sr-only">Nome</label>
      				<input type="text" name="nome" id="nome" class="form-control" placeholder="Nome" required autofocus>
      			<label for="nome" class="sr-only">Cognome</label>
      				<input type="text" name="cognome" id="cognome" class="form-control" placeholder="Cognome" required>
				<label for="ruolo" class="sr-only">Ruolo</label>
					<input type="text" name="ruolo" id="ruolo" class="form-control" placeholder="Ruolo" required>
				<label for="cell" class="sr-only">Numero di cellulare</label>
					<input type="text" name="cell" id="cell" class="form-control" placeholder="Numero di cellulare" require>
      			<input class="btn btn-lg btn-primary btn-block" name="submit" id="submit" type="submit" value="Genera firma">
      			<p class="mt-5 mb-3 text-muted">JEItaly &copy; 2019-2020</p>
    		</form>
    	</div>
    	<?php }?>

	<!-- JS import -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
	<script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
    </body>
</html>
